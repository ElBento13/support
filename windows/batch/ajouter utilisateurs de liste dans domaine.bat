@echo off
if [%1]==[] goto noarg
:: default password if input don't have.
set pass="P@ssw0rd"
for /F "tokens=* delims=;" %%a in (%1) do (
  if [%%c]==[] (
::    net user %%b "%pass%" /ADD /DOMAIN /FULLNAME:"%%a" /LOGONPASSWORDCHG:yes
    net user %%b "%pass%" /ADD /DOMAIN /FULLNAME:"%%a"
  ) else (
    net user %%b "%%c" /ADD /DOMAIN /FULLNAME:"%%a"
  )
)
exit

:noarg
echo Usage: drop on this .bat file a text file containing users to add to domain.
echo Or use the command line: %~nx0 "file with userlist.csv"
echo userlist file format:
echo Full Name;username;password
echo Full Name;username
pause
exit